﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurdMvcAll.Model;
namespace CurdMvcAll.Repository
{
   public interface IStudentInfo
    {
      Task< List<StateForStudent>> GetState();
        List<CityForStudent> GetCity(int ID);
        List<SubjectForStudent> GetSubject();

        List<StudentInfoModel> GetAllStudent();
        StudentInfoModel AddStudent(StudentInfoModel StudentInfoModel);
        SubjectSelectedByStudentModel AddSubjectByStudent(SubjectSelectedByStudentModel ssbs);

       List<SubjectSelectedByStudentModel> GetDetailsofSubjectStudent(int Studentid );

        StudentInfoModel Edit(int ID);



        int UpdateCity(StudentInfoModel UpdateStudentInfoModel);

    }
}
