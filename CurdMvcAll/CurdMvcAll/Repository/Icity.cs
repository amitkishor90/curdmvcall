﻿using System;
using System.Collections.Generic;
using System.Linq;
using CurdMvcAll.Model;
using System.Threading.Tasks;

namespace CurdMvcAll.Repository
{
    public interface Icity
    {
        CityModel GetCityId(int Id);
        int Delete(int id);
        List<CityModel> GetCity();
        int AddCity(CityModel AddCityModel);
        int UpdateCity(CityModel UpdateCityModel);
        List<StateModel> GetState();
       
    }
}
