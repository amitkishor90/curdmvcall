﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurdMvcAll.Model;

namespace CurdMvcAll.Repository
{
   public interface IState
    {
        StateModel GetStateById(int Id);
        int Delete(int id);
        List<StateModel> GetState();
        int AddState(StateModel AddStateModel);
        int UpdateStae(StateModel UpdateStateModel);
    }
}
