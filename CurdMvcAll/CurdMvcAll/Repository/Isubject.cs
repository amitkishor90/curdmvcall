﻿using CurdMvcAll.Model;
using System.Collections.Generic;

namespace CurdMvcAll.Repository
{
   public interface Isubject
    {
        SubjectModel GetSubjectById(int Id);
        int Delete(int id);
        List<SubjectModel> GetSubject();
        int AddSubject(SubjectModel SubjectAdd);
        int UpdateSubject(SubjectModel UPsubjectModel);
    }
}
