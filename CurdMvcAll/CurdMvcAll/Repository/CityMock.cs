﻿using CurdMvcAll.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurdMvcAll.DbModels;

namespace CurdMvcAll.Repository
{
    public class CityMock : Icity
    {
        private readonly AppDbContext _context;
        public CityMock(AppDbContext context)
        {
            this._context = context;
        }
        public int AddCity(CityModel AddCityModel)
        {
            var Ck_state = _context.tbl_City.Where(x => x.CityName == AddCityModel.CityName).ToList();
            if(Ck_state.Count <= 0)
            {
                var Ciy = new tbl_City()
                {
                    CityName = AddCityModel.CityName,
                    CID = AddCityModel.CID,
                    SID = AddCityModel.SID
                };

                  _context.tbl_City.Add(Ciy);
                _context.SaveChanges();
                return Ciy.CID;
            }
            return 0;
           

        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<CityModel> GetCity()
        {
          return  (from tc in _context.tbl_City.OrderByDescending(x=> x.CID)
             join ts in _context.tbl_State on tc.SID equals ts.Id 
             select new CityModel() 
             {
                 SID=ts.Id,
                 CID =tc.CID,
                 StateName=ts.StateName,
                 CityName=tc.CityName

             }).ToList();
        }

        public CityModel GetCityId(int Id)
        {
            return _context.tbl_City.Where(x => x.CID == Id).Select(city => new CityModel() {
               CID=city.CID,
               SID=city.SID,
               CityName=city.CityName

            }).FirstOrDefault();
          
        }

        public List<StateModel> GetState()
        {
            return _context.tbl_State.Select(st => new StateModel() { 
                Id=st.Id,
                StateName=st.StateName

            }).ToList();
        }

        public int UpdateCity(CityModel UpdateCityModel)
        {
            
            var ck = _context.tbl_City.Where(x => x.CityName == UpdateCityModel.CityName).ToList();

            if (ck.Count == 0)
            {
                var cityUp = new tbl_City()
                {
                    CityName = UpdateCityModel.CityName,
                    SID = UpdateCityModel.SID,
                    CID = UpdateCityModel.CID
                };

                var cityupdate = _context.tbl_City.Attach(cityUp);
                cityupdate.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
                return UpdateCityModel.CID;
            }

            return 0;
        }
    }
}
