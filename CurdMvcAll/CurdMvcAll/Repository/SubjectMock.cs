﻿using CurdMvcAll.DbModels;
using CurdMvcAll.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurdMvcAll.Repository
{
    public class SubjectMock : Isubject
    {
        private readonly AppDbContext _context;
        public SubjectMock(AppDbContext context)
        {
            this._context = context;
        }

        public int AddSubject(SubjectModel SubjectAdd)
        {
           var ck= _context.tbl_Subject.Where(x => x.SubjectName == SubjectAdd.SubjectName).ToList();
            if (ck.Count == 0)
            {
                var bc = new tbl_Subject()
                {
                    SubjectName = SubjectAdd.SubjectName
                };

                _context.tbl_Subject.Add(bc);
                _context.SaveChanges();
                return bc.ID;
            }
            return 0;
            
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<SubjectModel> GetSubject()
        {
            return _context.tbl_Subject.Select(Sunject => new SubjectModel()
            {
                SubjectName = Sunject.SubjectName,
                ID=Sunject.ID
            }).ToList();

              
        }

        public SubjectModel GetSubjectById(int Id) // for edit 
        {
            
          return  _context.tbl_Subject.Where(x => x.ID == Id).Select(subjectedit => new SubjectModel()
            {
                SubjectName = subjectedit.SubjectName,
                ID=subjectedit.ID
            }).FirstOrDefault();
            
        }

        public int UpdateSubject(SubjectModel UPsubjectModel)
        {
            var ck = _context.tbl_Subject.Where(x => x.SubjectName == UPsubjectModel.SubjectName).ToList();
            if (ck.Count == 0)
            {
                var bc = new tbl_Subject()
                {
                    SubjectName = UPsubjectModel.SubjectName,
                    ID= UPsubjectModel.ID

                };

                var subjectUpdate=   _context.tbl_Subject.Attach(bc);
                subjectUpdate.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
                return UPsubjectModel.ID;

            }
            return 0;
        }
    }
}
