﻿using CurdMvcAll.DbModels;
using CurdMvcAll.Model;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurdMvcAll.Repository
{
    public class StudentinfoMock : IStudentInfo
    {
        private readonly AppDbContext _context;
        public StudentinfoMock(AppDbContext context)
        {
            this._context = context;
        }
        StudentInfoModel smi = new StudentInfoModel();
        public async  Task<List<StateForStudent>>  GetState()  // use Asynchronous Programming
        {
            return await _context.tbl_State.Select( a=> new StateForStudent() { 

                Id=a.Id,
                StateName=a.StateName
            }).ToListAsync();

        }

        public List<CityForStudent> GetCity(int ID)
        {
            return _context.tbl_City.Where(x=> x.SID==ID).Select(c => new CityForStudent()
            {
                CityName=c.CityName,
                Id=c.CID
            }).ToList();
        }

        public List<SubjectForStudent> GetSubject()
        {
            return _context.tbl_Subject.Select(z => new SubjectForStudent() {
                ID=z.ID,
                SubjectName=z.SubjectName,
                IsCheck=false
                
            }).ToList();
        }

        public StudentInfoModel AddStudent(StudentInfoModel StudentInfoModel)
        {
            var Studentobj = new tbl_StudentInfo()
            {
                Name = StudentInfoModel.Name,
                EmilID=StudentInfoModel.EmilID,
                Gender=StudentInfoModel.Gender,
                MobileNo=StudentInfoModel.MobileNo,
                City=StudentInfoModel.City,
                State= StudentInfoModel.State,
                DateOfBirthday=Convert.ToString( StudentInfoModel.DateOfBirthday),
                StudentImage= StudentInfoModel.StudentImage

            };

            _context.tbl_StudentInfo.Add(Studentobj);
            _context.SaveChanges();
            StudentInfoModel.SID= Studentobj.SID;
            return StudentInfoModel;


        }

        public List<StudentInfoModel> GetAllStudent()
        {
           return (from tbs in _context.tbl_StudentInfo
                     join st in _context.tbl_State on tbs.State equals st.Id
                     join ct in _context.tbl_City on tbs.City equals ct.CID  orderby tbs.SID descending
                     select new StudentInfoModel()
                     {
                         SID=tbs.SID,
                         Name =tbs.Name,
                         EmilID=tbs.EmilID,
                         Gender=tbs.Gender,
                         MobileNo=tbs.MobileNo,
                         StsteJoin=st.StateName,
                         CityJoin =ct.CityName,
                         DateOfBirthday= Convert.ToDateTime( tbs.DateOfBirthday),
                         StudentImage =tbs.StudentImage

                     }).ToList();

            
        }

        public SubjectSelectedByStudentModel AddSubjectByStudent(SubjectSelectedByStudentModel ssbs)
        {
            var StudentSubjectSelected = new tbl_SubjectSelectedByStudent()
            {
                StudentID = ssbs.StudentID,
                SybjectID = ssbs.SybjectID,
            };

            _context.tbl_SubjectSelectedByStudent.Add(StudentSubjectSelected);
            _context.SaveChanges();
            StudentSubjectSelected.ID = StudentSubjectSelected.ID;
            return ssbs;
        }

        public List< SubjectSelectedByStudentModel> GetDetailsofSubjectStudent(int Studentid)
        {
            return ( from tbs in _context.tbl_StudentInfo
            join Ssd in _context.tbl_SubjectSelectedByStudent on tbs.SID equals Ssd.StudentID
            join mts in _context.tbl_Subject on Ssd.SybjectID equals mts.ID
            where Ssd.StudentID == Studentid
            select new SubjectSelectedByStudentModel()
            {
                SubjectName= mts.SubjectName,
            }).ToList();
        }

        public StudentInfoModel Edit(int ID)
        {
            var editwork = (_context.tbl_StudentInfo.Where(x => x.SID == ID).Select(ts => new StudentInfoModel()
            {
                SID = ts.SID,
                Name = ts.Name,
                EmilID = ts.EmilID,
                Gender = ts.Gender,
                MobileNo = ts.MobileNo,
                State = ts.State,
                City = ts.City,
                DateOfBirthday = Convert.ToDateTime(ts.DateOfBirthday),
                ExistingphotoPath = ts.StudentImage,
                SubjectForStudentEdit = (from ss in _context.tbl_SubjectSelectedByStudent
                                         join tms in _context.tbl_Subject on ss.SybjectID equals tms.ID
                                         where ss.StudentID == ID
                                         select new SubjectForStudentEdit()
                                         {
                                             SubjectName = tms.SubjectName,
                                             IsCheck = true,
                                             subjectID = ss.SybjectID

                                         }).ToList()



            }).FirstOrDefault()) ;


            return editwork;
        }

        public int UpdateCity(StudentInfoModel UpdateStudentInfoModel)
        {
            throw new NotImplementedException();
        }
    }
}
