﻿using CurdMvcAll.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurdMvcAll.DbModels;

namespace CurdMvcAll.Repository
{
    public class StateMock : IState
    {
        private readonly AppDbContext _context;
        public StateMock(AppDbContext context)
        {
            this._context = context;
        }
        public int AddState(StateModel AddStateModel)
        {
            var Ck_state = _context.tbl_State.Where(x => x.StateName == AddStateModel.StateName).ToList();
            if(Ck_state.Count >= 0)
            {
                var St = new tbl_State()
                {
                    StateName = AddStateModel.StateName,
                    Id = AddStateModel.Id
                };
                _context.tbl_State.Add(St);
                _context.SaveChanges();
                return St.Id;
            }
            return 0;
           
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<StateModel> GetState()
        {
          return  _context.tbl_State.Select(state => new StateModel()
            {
                StateName=state.StateName,
                Id =state.Id
            }).ToList();
        }

        public StateModel GetStateById(int Id)
        {
            return _context.tbl_State.Where(x=> x.Id==Id).Select(state => new StateModel()
            {
                StateName = state.StateName,
                Id = state.Id
            }).FirstOrDefault();
        }

        public int UpdateStae(StateModel UpdateStateModel)
        {
            var ck = _context.tbl_State.Where(x => x.StateName == UpdateStateModel.StateName).ToList();
            if (ck.Count == 0)
            {
                var bc = new tbl_State()
                {
                    StateName = UpdateStateModel.StateName,
                    Id = UpdateStateModel.Id

                };

                var subjectUpdate = _context.tbl_State.Attach(bc);
                subjectUpdate.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
                return UpdateStateModel.Id;

            }
            return 0;
        }
    }
}
