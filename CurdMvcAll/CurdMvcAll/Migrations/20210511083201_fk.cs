﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CurdMvcAll.Migrations
{
    public partial class fk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CityName",
                table: "tbl_City",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "tbl_StudentInfo",
                columns: table => new
                {
                    SID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EmilID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MobileNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<int>(type: "int", nullable: false),
                    City = table.Column<int>(type: "int", nullable: false),
                    DateOfBirthday = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_StudentInfo", x => x.SID);
                });

            migrationBuilder.CreateTable(
                name: "tbl_SubjectSelectedByStudent",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StudentID = table.Column<int>(type: "int", nullable: false),
                    SybjectID = table.Column<int>(type: "int", nullable: false),
                    StudentIDFK = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_SubjectSelectedByStudent", x => x.ID);
                    table.ForeignKey(
                        name: "FK_tbl_SubjectSelectedByStudent_tbl_StudentInfo_StudentIDFK",
                        column: x => x.StudentIDFK,
                        principalTable: "tbl_StudentInfo",
                        principalColumn: "SID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tbl_SubjectSelectedByStudent_StudentIDFK",
                table: "tbl_SubjectSelectedByStudent",
                column: "StudentIDFK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tbl_SubjectSelectedByStudent");

            migrationBuilder.DropTable(
                name: "tbl_StudentInfo");

            migrationBuilder.AlterColumn<string>(
                name: "CityName",
                table: "tbl_City",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");
        }
    }
}
