﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CurdMvcAll.DbModels
{
    public class tbl_StudentInfo
    {
        [Key]
        [Column(Order = 1)]
        public int SID { get; set; }
        public String Name { get; set; }
        public String EmilID { get; set; }
        public String Gender { get; set; }
        public String MobileNo { get; set; }
        public int State { get; set; }
        public int City { get; set; }
        public String DateOfBirthday { get; set; }

        public String StudentImage { get; set; }



        [ForeignKey("StudentIDFK")]
        public ICollection<tbl_SubjectSelectedByStudent> tblsubjectselect { get; set; }



    }
}
