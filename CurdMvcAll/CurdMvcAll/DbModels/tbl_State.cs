﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurdMvcAll.DbModels
{
    public class tbl_State
    {
        public int Id { get; set; }
        public String StateName { get; set; }
    }
}
