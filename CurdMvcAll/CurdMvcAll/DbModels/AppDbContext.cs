﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurdMvcAll.DbModels
{
    public class AppDbContext:DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<tbl_Subject> tbl_Subject { get; set; }
        public DbSet<tbl_State> tbl_State { get; set; }
        public DbSet<tbl_City> tbl_City { get; set; }
        public DbSet<tbl_SubjectSelectedByStudent> tbl_SubjectSelectedByStudent { get; set; }
        public DbSet<tbl_StudentInfo> tbl_StudentInfo { get; set; }
    }
}
