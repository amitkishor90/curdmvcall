﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CurdMvcAll.DbModels
{
    public class tbl_SubjectSelectedByStudent
    {
        [Key]
        [Column(Order = 1)]
        public int ID { get; set; }

        [ForeignKey("StudentIDFK")]
        public int StudentID { get; set; }
        public int SybjectID { get; set; }
       
    }
}
