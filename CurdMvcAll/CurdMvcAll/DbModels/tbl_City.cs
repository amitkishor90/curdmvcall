﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CurdMvcAll.DbModels
{
    public class tbl_City
    {
        [Key]
        public int CID { get; set; }
        [Required(ErrorMessage = "Select State  Name")]
        public int SID { get; set; }
        [Required(ErrorMessage = "Enter City  Name")]
        public string CityName { get; set; }
       
    }
}
