﻿using System;
using System.ComponentModel.DataAnnotations;
 

namespace CurdMvcAll.Model
{
    public class StateModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter Stae Name")]
        public String StateName { get; set; }
    }
}
