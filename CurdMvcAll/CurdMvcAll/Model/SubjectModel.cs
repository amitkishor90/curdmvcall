﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace CurdMvcAll.Model
{
    public class SubjectModel
    {
        public int ID { get; set; }
        [Required (ErrorMessage ="Enter Subject Name")]
        public string SubjectName { get; set; }
    }
}
