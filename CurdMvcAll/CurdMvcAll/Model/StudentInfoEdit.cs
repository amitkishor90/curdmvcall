﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CurdMvcAll.CustomValidation;
using Microsoft.AspNetCore.Http;

namespace CurdMvcAll.Model
{
    public class StudentInfoEdit
    {
        public int SID { get; set; }
        [Required(ErrorMessage = "Enter Name")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter email address")]
        [Display(Name = "Email Address")]
        [EmailAddress]
        public string EmilID { get; set; }

        [Required(ErrorMessage = "*")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "Please enter phone number")]
        [Phone]
        public string MobileNo { get; set; }

        [Required(ErrorMessage = "Please select  State ")]
        public int State { get; set; }
        [Required(ErrorMessage = "Please select  city ")]
        public int City { get; set; }

        [Required(ErrorMessage = "Enter  DOB ")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirthday { get; set; }
        public string StudentImage { get; set; }

        [Required(ErrorMessage = "Please select a file.")]
        [DataType(DataType.Upload)]
        [MaxFileSize(5 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png" })]
        public IFormFile Photo { get; set; }

        public List<StateForStudentEdit> StateModelListEdit { get; set; }
    }

    public partial class StateForStudentEdit
    {
        public int Id { get; set; }
        public String StateName { get; set; }
    }
}
