﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CurdMvcAll.Model
{
    public class CityModel
    {
        
        public int CID { get; set; }

        [Required(ErrorMessage = "Select state Name")]
        public int SID { get; set; }
       
        public string StateName { get; set; }

        [Required(ErrorMessage = "Enter City Name")]
        public string CityName { get; set; }

        public List<StateModel> StateModelList { get; set; }
    }
}
