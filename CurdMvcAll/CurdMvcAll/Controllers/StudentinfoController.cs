﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurdMvcAll.Model;
using CurdMvcAll.DbModels;
using CurdMvcAll.Repository;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text.Json;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace CurdMvcAll.Controllers
{
    public class StudentinfoController : Controller
    {
        private readonly IStudentInfo _IStudentInfo;
        private readonly IHostingEnvironment _IHostingEnvironment;
        public StudentinfoController(IStudentInfo state, 
            IHostingEnvironment hostingEnvironment)       //Implemented Dependency Injection
        {
            _IStudentInfo = state;
            this._IHostingEnvironment = hostingEnvironment; 
        }
        [HttpGet]
        public async Task< IActionResult >Studentinfo(bool suss, int id)
        {
            ViewBag.susscess = suss;
              var Studentmodel = new StudentInfoModel();
            Studentmodel.StateModelList1 = await _IStudentInfo.GetState();   //Asynchronous 
            Studentmodel.SubjectModelList = _IStudentInfo.GetSubject();
            return View(Studentmodel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> StudentinfoAsync(StudentInfoModel model)
        {
            if (ModelState.IsValid)
            {
                string uniqFile = ProceessUploadFile(model);
                StudentInfoModel student = new StudentInfoModel()
                {
                    Name = model.Name,
                    EmilID = model.EmilID,
                    Gender = model.Gender,
                    MobileNo = model.MobileNo,
                    State = model.State,
                    City = model.City,
                    DateOfBirthday =  model.DateOfBirthday,
                    StudentImage = uniqFile
                };
                StudentInfoModel s= _IStudentInfo.AddStudent(student);
                foreach (var item in model.SubjectModelList)  // for subjecct 
                {
                    if (item.IsCheck == true)
                    {
                        var Sid=  s.SID;
                        var idd = item.ID;
                        SubjectSelectedByStudentModel sub = new SubjectSelectedByStudentModel()
                        {
                            StudentID = s.SID, // student id 
                            SybjectID = item.ID,// for subject id 

                        };

                        _IStudentInfo.AddSubjectByStudent(sub);
                    }
                }
                int a = s.SID;
                if (a >0)
                {

                    return RedirectToAction(nameof(Studentinfo), new { suss = true, id = model.SID });
                }
                else
                {
                    ViewBag.susscess = true;
                }


                return RedirectToAction("Studentinfo");
            }
            var Studentmodel = new StudentInfoModel();
            Studentmodel.StateModelList1 =  await  _IStudentInfo.GetState();
            Studentmodel.SubjectModelList = _IStudentInfo.GetSubject();
            return View(Studentmodel);
            
        }

        private string ProceessUploadFile(StudentInfoModel model1)
        {
            string uniqueFilename = null;
            if (model1.Photo != null)
            {
                string uploadfolder = Path.Combine(_IHostingEnvironment.WebRootPath + "/" + "Images");
                uniqueFilename = Guid.NewGuid().ToString() + "_" + model1.Photo.FileName;
                string filepath = Path.Combine(uploadfolder, uniqueFilename);

                using (var filestream = new FileStream(filepath, FileMode.Create))
                {
                    model1.Photo.CopyTo(filestream);
                }
            }

            return uniqueFilename;
        }






        [HttpGet]
        public JsonResult LoadCity(int id)
        {
            List<CityForStudent> cfs = new List<CityForStudent>();
             cfs = _IStudentInfo.GetCity(id);
            cfs.Insert(0, new CityForStudent { Id = 0, CityName = "Select" });
            return Json(cfs);
        }


        public IActionResult GetAllstu()
        {
            return View(_IStudentInfo.GetAllStudent());
        }

        [HttpGet]
        public IActionResult Edit(int id)  // working 
        {
            StudentInfoModel s = new StudentInfoModel();

            var Editrecoad = new StudentInfoModel();
            Editrecoad = _IStudentInfo.Edit(id);
            Editrecoad.SubjectModelList = _IStudentInfo.GetSubject();

            for (int i = 0; i < Editrecoad.SubjectModelList.Count; i++)
            {
                if (!string.IsNullOrEmpty(Editrecoad.SubjectForStudentEdit.ToString()))
                {
                    foreach (var item in Editrecoad.SubjectForStudentEdit)
                    {
                        if (item.subjectID == Editrecoad.SubjectModelList[i].ID)
                        {
                            Editrecoad.SubjectModelList[i].IsCheck = true;
                        }
                    }
                }
                else
                {

                }
            }

            Editrecoad.StateModelList1 = _IStudentInfo.GetState();
            s.State = Editrecoad.State;
            Editrecoad.cityModelList1 = _IStudentInfo.GetCity(s.State);
            s.City = Editrecoad.City;

            if (Editrecoad == null)
            {
                Response.StatusCode = 404;
                return View("home/EmployeNotfound", id);
            }
            return View(Editrecoad);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(StudentInfoModel model)  // update work 
        {
            if (ModelState.IsValid)
            {
            }
                return View();
        }


        [HttpGet]
        public JsonResult GetSubjectDetailsByStudent(int studentId)
        {
            List<SubjectSelectedByStudentModel> sub = new List<SubjectSelectedByStudentModel>();
            sub = _IStudentInfo.GetDetailsofSubjectStudent(studentId);
            return Json(sub);
        }
    }
}
