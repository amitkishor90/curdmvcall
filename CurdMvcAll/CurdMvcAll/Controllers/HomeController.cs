﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurdMvcAll.Model;
using CurdMvcAll.Repository;
using CurdMvcAll.DbModels;
using Newtonsoft.Json;

namespace CurdMvcAll.Controllers
{
    public class HomeController : Controller
    {
        private readonly Isubject _sub;
        public HomeController(Isubject sub)    // Dependency Injection
        {
            _sub = sub;
        }
        public IActionResult Index(bool isSuccess=false, int id=0)
        {
            //ViewBag.isSuccess = isSuccess;
            //ViewBag.Sid = id;

            if (isSuccess == false)
            {
                return View();
            }
            else
            {
                
                return Json(new { s=true });
            }
            
        }

        [HttpPost]
        public IActionResult Index(SubjectModel Sub)  //66
        {
            if (ModelState.IsValid)
            {
              int a=  _sub.AddSubject(Sub);
                if (a > 0)
                {
                    return Json(new { f = true });
                }
                else
                {
                    return Json(new { f = false });
                }
            }
            else
            {
                return Json(new { f = false });
            }
        }


       [HttpGet]
        public IActionResult AllSubject()
        {
             List<SubjectModel> sub = _sub.GetSubject();
             return View(sub);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
           
           SubjectModel subjectModelobject= _sub.GetSubjectById(id);
            if (subjectModelobject == null)
            {
                Response.StatusCode = 404;
                return View("EmployeNotfound", id);
            }
            return View(subjectModelobject);
        }

        [HttpPost]
        public IActionResult Edit(SubjectModel sub)
        {
            if (ModelState.IsValid)
            {
                int a = _sub.UpdateSubject(sub);
                if (a > 0)
                {
                    ViewBag.updateMassage = "Subject name Update";
                    return RedirectToAction(nameof(Index), new { isSuccess = true, sunjectid = a });
                }
                else
                {
                    return Json(new { f = false });
                }
            }
            else
            {
                return Json(new { f = false });
            }
        }
    }
}
