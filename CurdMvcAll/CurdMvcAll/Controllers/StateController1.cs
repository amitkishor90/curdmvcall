﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurdMvcAll.Repository;
using CurdMvcAll.Model;

namespace CurdMvcAll.Controllers
{
    public class StateController1 : Controller
    {
        private readonly IState _state;
        public StateController1(IState state)
        {
            _state = state;
        }
        [HttpGet]
        public IActionResult State(bool suss , int id)
        {
            ViewBag.issuss = suss;
            return View();
        }

        [HttpPost]
        public IActionResult State(StateModel  StaeModel)
        {
            if (ModelState.IsValid)
            {
                int s = _state.AddState(StaeModel);
                if (s > 0)
                {
                    return RedirectToAction(nameof(State),new { suss =true,id= StaeModel.Id});
                }
            }
            return View();
        }


        public IActionResult StateList()
        {
           List<StateModel> sm = _state.GetState();
            return View(sm);
        }
        [HttpGet]
        public ViewResult EditState(int id)
        {
            StateModel SM = _state.GetStateById(id);
            if (SM == null)
            {
                Response.StatusCode = 404;
                return View("/home/EmployeNotfound", id);
            }
            return View(SM);
        }

       
        [HttpPost]
        public IActionResult EditState(StateModel SM)
        {
            int updatevalue = _state.UpdateStae(SM);
            if(updatevalue > 0)
            {
                ViewBag.update = true;
                // return RedirectToAction(nameof(State), new { suss = true, id = SM.Id });
                return View("State");
            }
            return View();
        }
    }
}
