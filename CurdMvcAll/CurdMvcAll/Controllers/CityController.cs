﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurdMvcAll.DbModels;
using CurdMvcAll.Model;
using CurdMvcAll.Repository;

namespace CurdMvcAll.Controllers
{
    public class CityController : Controller
    {
        private readonly Icity _Icity;
        public CityController(Icity City)
        {
            _Icity = City;
        }
        CityModel c = new CityModel();
        [HttpGet]
        public IActionResult AddCity(bool suss, int id)
        {
            ViewBag.SaveCityName = suss;
            c.StateModelList = _Icity.GetState();
            return View(c);
        }


        [HttpPost]
        public IActionResult AddCity(CityModel model)
        {
            if (ModelState.IsValid)
            {
                int a = _Icity.AddCity(model);
                if( a>0)
                {
                    
                    return RedirectToAction(nameof(AddCity), new { suss = true, id = model.CID });
                }
                else
                {
                    ViewBag.SaveCityName = false;
                }
            }
            c.StateModelList = _Icity.GetState();
            return View(c);
        }

        [HttpGet]
        public IActionResult GetCity()
        {
            List<CityModel> cy = _Icity.GetCity();
            return View(cy);
        }



        [HttpGet]
        public ViewResult EditCity(int id)
        {
            var editselect= _Icity.GetCityId(id);

            c.CityName = editselect.CityName;
            c.SID = editselect.SID;
            c.CID = editselect.CID;
            c.StateModelList = _Icity.GetState();
            
            
           
            if (c == null)
            {
                Response.StatusCode = 404;
                return View("home/EmployeNotfound", id);
            }
            return View(c);
        }



        [HttpPost]
        public ViewResult EditCity(CityModel model)
        {

            int a = _Icity.UpdateCity(model);
            if (a > 0)
            {
                ViewBag.update = true;
                // return RedirectToAction(nameof(State), new { suss = true, id = SM.Id });
                c.StateModelList = _Icity.GetState();
                return View("AddCity",c);
            }
            c.StateModelList = _Icity.GetState();
            return View(c);
        }

    }
}
