using CurdMvcAll.DbModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurdMvcAll.Repository;

namespace CurdMvcAll
{
    public class Startup
    {
        private IConfiguration _config;
        public Startup(IConfiguration configuration)
        {
            _config = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContextPool<AppDbContext>(
                opctions => opctions.UseSqlServer(_config.GetConnectionString("MVCALLCORE")));

            services.AddControllersWithViews().AddXmlSerializerFormatters();
            //  services.AddSingleton<IemployeeRepostory, MockEmployeeREpository>();
            services.AddScoped<Isubject, SubjectMock>();
            services.AddScoped<IState, StateMock>();
            services.AddScoped<Icity, CityMock>();
            services.AddScoped<IStudentInfo, StudentinfoMock>();

            services.AddRazorPages();

#if DEBUG
            services.AddRazorPages().AddRazorRuntimeCompilation();   //microsoft.aspnetcore.mvc.razor.runtimecompilation  this is nuget package
#endif
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
               // app.UseExceptionHandler("/Error");
                app.UseStatusCodePagesWithReExecute("/Error/{0}"); // for enter bad url 

            }
            else
            {
                app.UseExceptionHandler("/Error/{0}");
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
